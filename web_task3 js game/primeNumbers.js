function isPrime(number) {
    for(var i = 2; i < number; i++) {
        if(number % i === 0) {
            return false;
        }
    }
    return true;
}

function findPrimes(min, max) {
    for(let i=min; i<=max; i++){
        if(isPrime(i)) console.log(i);
    }
}

findPrimes(5, 20);