function game() {
    alert('Let`s begin!');
    let min = 1;
    let max = 5;
    let generateRandNum = Math.floor(Math.random() * (max - min + 1));
    let guessNum = 3;
    let userInput;
    while(guessNum > 0) {
        userInput = prompt('Please suggest number between 1 and 5. Attempts:' + guessNum);
        if (userInput == generateRandNum) {
            alert(`Congratulations! You are winner! You suggested right number: ${generateRandNum}`);
            break;
        }
        guessNum--;
        if(guessNum == 0){
            alert("Today is not your day.");
            break;
        }
    }
}

let beginGame = confirm('Do you want to play?');
if(beginGame) {
    game();  
} else {
    alert('Not today\n...');
}
